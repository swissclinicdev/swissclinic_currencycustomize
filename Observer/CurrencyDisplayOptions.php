<?php
namespace Swissclinic\CurrencyCustomize\Observer;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\ObserverInterface;

class CurrencyDisplayOptions implements ObserverInterface
{
    const XML_PATH_SYMBOL_POSITION = 'currency/options/symbol_position';

    /**
     * @var ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * @param ScopeConfigInterface
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * Generate options for currency displaying with custom currency symbol
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $position = $this->_scopeConfig->getValue(self::XML_PATH_SYMBOL_POSITION,'store');
        if(is_numeric($position)) {
            $currencyOptions = $observer->getEvent()->getCurrencyOptions();
            $currencyOptions->setData('position', (int)$position);
        }
        return $this;
    }
}
