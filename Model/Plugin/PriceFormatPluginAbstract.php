<?php
namespace Swissclinic\CurrencyCustomize\Model\Plugin;

use Swissclinic\CurrencyCustomize\Model\ConfigInterface;
use Swissclinic\CurrencyCustomize\Model\PricePrecisionConfigTrait;

abstract class PriceFormatPluginAbstract
{

    use PricePrecisionConfigTrait;

    /** @var ConfigInterface  */
    protected $moduleConfig;

    /**
     * @param \Swissclinic\CurrencyCustomize\Model\ConfigInterface $moduleConfig
     */
    public function __construct(
        ConfigInterface $moduleConfig
    ) {
        $this->moduleConfig  = $moduleConfig;
    }
}
