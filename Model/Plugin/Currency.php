<?php
namespace Swissclinic\CurrencyCustomize\Model\Plugin;

use Magento\Framework\CurrencyInterface;

class Currency extends PriceFormatPluginAbstract
{

    /**
     * {@inheritdoc}
     *
     * @param \Magento\Framework\CurrencyInterface $subject
     * @param array                                ...$args
     *
     * @return array
     */
    public function beforeToCurrency(
        CurrencyInterface $subject,
        ...$arguments
    ) {
        if ($this->getConfig()->isEnable()) {
            $arguments[1]['precision'] = $this->getPricePrecision();
        }
        return $arguments;
    }
}
