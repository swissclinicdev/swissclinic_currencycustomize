<?php


namespace Swissclinic\CurrencyCustomize\Model\Config\Source;
use Magento\Framework\Currency;

class CurrencySymbolPosition implements \Magento\Framework\Data\OptionSourceInterface
{
    private $_options;

    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray() {
        if (!$this->_options) {
            $this->_options = [
                ['value' => Currency::STANDARD, 'label' => __('Standard')],
                ['value' => Currency::RIGHT, 'label' => __('Right')],
                ['value' => Currency::LEFT, 'label' => __('Left')]
            ];
        }

        return $this->_options;
    }
}